import argparse
import json
import dropbox
from dropbox import DropboxOAuth2FlowNoRedirect
import os
import time
from datetime import datetime

parser = argparse.ArgumentParser(description = "Log files to Dropbox.")
parser.add_argument("configfile", help="JSON file with Dropbox keys.")
parser.add_argument("filepath", help="Path to directory of files to upload.")
# parser.add_argument("dboxpath", help="Dropbox target location.")
parser.add_argument("-m", "--moveto", help="Local path to move files to.")
parser.add_argument("-d", "--delete", help="Delete files after upload.", 
	action="store_true")
parser.add_argument("-l", "--logfile", help="Log file path for program output.")
parser.add_argument("-v", "--verbose", help="Verbose output",
    action="store_true")
args = parser.parse_args()

if(args.verbose): print("Opening config file at " + args.configfile)
with open(args.configfile) as configFile:
	config = json.load(configFile)

if(args.logfile):
	with open(args.logfile, 'a') as logFile:
		logFile.write(datetime.now().strftime("%Y-%m-%d %H:%M:%S") + 
			" Script started.\n")

if(config["TOKEN"] == ''):
	if(args.verbose): print("Empty token value.")
	auth_flow = DropboxOAuth2FlowNoRedirect(config["APP_KEY"], 
		config["APP_SECRET"])
	authorize_url = auth_flow.start()
	print("1. Go to: " + authorize_url)
	print("2. Click \"Allow\" (you might have to log in first).")
	print("3. Copy the authorization code.")
	auth_code = input("Enter the authorization code here: ").strip()
	
	if(args.verbose): print("Authorizing application.")
	try:
		oauth_result = auth_flow.finish(auth_code)
		config["TOKEN"] = oauth_result.access_token
		with open(args.configfile, 'w') as configFile:
			configFile.write(json.dumps(config, indent=4))
	except Exception as e:
		print('Error: %s' % (e,))
		# TODO: Handle 5xx codes if authorization fails.

dbx = dropbox.Dropbox(config["TOKEN"])
user = dbx.users_get_current_account()
if(args.verbose): print("Using account for " + user.email)

fileList = os.listdir(args.filepath)
filecount = len(fileList)
if(args.verbose): print("Processing " + str(filecount) + " files.")
uploadcount = 0
for filename in fileList:
	t0 = time.time()
	# path = "%s/%s" % (args.dboxpath, filename)
	path = '/' + filename
	# if(args.verbose): print("Uploading file " + path)
	with open(os.path.join(args.filepath, filename), "rb") as dataFile:
		fileData = dataFile.read()
		try:
			response = dbx.files_upload(fileData, path) 
			uploadcount = uploadcount + 1
		except dropbox.exceptions.ApiError as e:
			print("Upload error", e)
	# if(args.verbose): print("File uploaded as ", response.name.encode("utf8"))
	t1 = time.time()
	if(args.verbose): print("File {0}/{1} uploaded as {2} in {3:.2f} seconds.".
		format(uploadcount, filecount, response.name, (t1-t0)))
	if(args.moveto):
		os.rename(os.path.join(args.filepath, filename), 
			os.path.join(args.moveto, filename))
	elif(args.delete):
		os.remove(os.path.join(args.filepath, filename))

if(args.logfile):
	with open(args.logfile, 'a') as logFile:
		logFile.write(datetime.now().strftime("%Y-%m-%d %H:%M:%S") + 
			" Uploaded {0} files.\n".format(uploadcount))

